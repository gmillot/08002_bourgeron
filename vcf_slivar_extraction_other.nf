/*
#########################################################################
##                                                                     ##
##     vcf_slivar_extraction.nf                                        ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################
*/


//////// Arguments of nextflow run

params.modules = ""

//////// end Arguments of nextflow run


//////// Variables

config_file = file("${projectDir}/vcf_slivar_extraction.config")
log_file = file("${launchDir}/.nextflow.log")
modules = params.modules // remove the dot -> can be used in bash scripts

//////// end Variables


//////// Variables from config.file that need to be modified

sample_path_test = file("${sample_path}") // to test if exist below

//////// end Variables from config.file that need to be modified


//////// Channels

//// used once

vcf_ch1 = Channel.fromPath("${sample_path}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message
affected_patients_ch = Channel.value("${affected_patients}")
unaffected_patients_ch = Channel.value("${unaffected_patients}")
header_other_ch = Channel.value("${header_other}")
patient_ch = Channel.value(["${affected_patients}", "${unaffected_patients}"]) // create two entries, see https://www.nextflow.io/docs/latest/channel.html#from
patient_name_ch = Channel.value(["affected_patients", "unaffected_patients"]) // create two entries, see https://www.nextflow.io/docs/latest/channel.html#from

//// end used once

//////// end Channels



//////// Checks

if(system_exec == 'local' || system_exec == 'slurm'){
    if(system_exec == 'local'){
        def file_exists = sample_path_test.exists()
        if( ! file_exists){
            error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID sample_path PARAMETER IN nextflow.config FILE: ${sample_path}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
        }
    }
}else{
    error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID system_exec PARAMETER IN nextflow.config FILE: ${system_exec}\n\n========\n\n"
}

//////// end Checks



//////// Processes


process WorkflowVersion { // create a file with the workflow version in out_path
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}", mode: 'copy'
    cache 'false'

    output:
    file "Run_info.txt"

    script:
    """
    echo "Project (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} remote -v | head -n 1) > Run_info.txt # works only if the main script run is located in a directory that has a .git folder, i.e., that is connected to a distant repo
    echo "Git info (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} describe --abbrev=10 --dirty --always --tags) >> Run_info.txt # idem. Provide the small commit number of the script and nextflow.config used in the execution
    echo "Cmd line: ${workflow.commandLine}" >> Run_info.txt
    echo "execution mode": ${system_exec} >> Run_info.txt
    modules=$modules # this is just to deal with variable interpretation during the creation of the .command.sh file by nextflow. See also \$modules below
    if [[ ! -z \$modules ]] ; then
        echo "loaded modules (according to specification by the user thanks to the --modules argument of main.nf)": ${modules} >> Run_info.txt
    fi
    echo "Manifest's pipeline version: ${workflow.manifest.version}" >> Run_info.txt
    echo "result path: ${out_path}" >> Run_info.txt
    echo "nextflow version: ${nextflow.version}" >> Run_info.txt
    echo -e "\\n\\nIMPLICIT VARIABLES:\\n\\nlaunchDir (directory where the workflow is run): ${launchDir}\\nprojectDir (directory where the main.nf script is located): ${projectDir}\\nworkDir (directory where tasks temporary files are created): ${workDir}" >> Run_info.txt
    echo -e "\\n\\nUSER VARIABLES:\\n\\nout_path: ${out_path}\\nin_path: ${in_path}" >> Run_info.txt
    """
}
//${projectDir} nextflow variable
//${workflow.commandLine} nextflow variable
//${workflow.manifest.version} nextflow variable
//Note that variables like ${out_path} are interpreted in the script block



process slivar {
    label 'slivar' // see the withLabel: bash in the nextflow config file 
    publishDir path: "${out_path}", mode: 'copy', overwrite: false
    cache 'false'

    //no channel input here for the vcf, because I do not transform it
    input:
    file vcf_gz from vcf_ch1
    val affected_patients from affected_patients_ch
    val unaffected_patients from unaffected_patients_ch
    val header_other from header_other_ch
    // see the scope for the use of affected_patients which is already a variable from .config file

    output:
    file vcf into vcf_ch2
    file "slivar_log.txt"

    script:
    """
    slivar expr --js ${fun_path} \
    -g gnomad.hg38.genomes.v3.fix.zip \
    --vcf $vcf 
    --ped $ped \
    --info "INFO.gnomad_popmax_af < 0.01 && variant.FILTER == 'PASS'" \
    --trio "example_denovo:denovo(kid, dad, mom)" \
    --family-expr "denovo:fam.every(segregating_denovo)" \
    --trio "custom:kid.het && mom.het && dad.het && kid.GQ > 20 && mom.GQ > 20 && dad.GQ > 20" \
    --pass-only
    """
    // write between "" (example "$unaffected_patients") to make a single argument when the variable is made of several values separated by a space. Otherwise, several arguments will be considered
    // warning: ${in_path}/${sample_name} instead of ${vcf_gz} does not work, because the file used is not here anymore (do not use path in nextflow)
}

process slivar2tsv {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir path: "${out_path}", mode: 'copy', overwrite: true
    cache 'false'

    //no channel input here for the vcf, because I do not transform it
    input:
    file vcf_gz from vcf_ch2
    // see the scope for the use of affected_patients which is already a variable from .config file

    output:
    file "final.tsv"
    file "slivar2tsv_log.txt"

    script:
    """
    slivar tsv -p $ped \
    -s denovo -s x_recessive \
    -c CSQ \
    -i gnomad_popmax_af -i gnomad_nhomalt \
    -g gene_desc.txt -g clinvar_gene_desc.txt \
    $vcf > final.tsv 
    """
    // write between "" (example "$unaffected_patients") to make a single argument when the variable is made of several values separated by a space. Otherwise, several arguments will be considered
    // warning: ${in_path}/${sample_name} instead of ${vcf_gz} does not work, because the file used is not here anymore (do not use path in nextflow)
}



process Backup {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}", mode: 'copy', overwrite: false // since I am in mode copy, all the output files will be copied into the publishDir. See \\wsl$\Ubuntu-20.04\home\gael\work\aa\a0e9a739acae026fb205bc3fc21f9b
    cache 'false'

    input:
    file config_file
    file log_file

    output:
    file "${config_file}" // warning message if we use file config_file
    file "${log_file}" // warning message if we use file log_file
    file "Log_info.txt"

    script:
    """
    echo -e "full .nextflow.log is in: ${launchDir}\nThe one in the result folder is not complete (miss the end)" > Log_info.txt
    """
}


//////// end Processes
