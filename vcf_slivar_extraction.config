/*
#########################################################################
##                                                                     ##
##     vcf_slivar_extraction.config                                    ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################
*/


//////// variables that will be used only in the main.nf

// variables exported to the main.nf environment. See https://www.nextflow.io/docs/latest/config.html#scope-env
env {
    git_path="https://gitlab.pasteur.fr/Gael/08002_bourgeron/"
    //in_path="/pasteur/gaia/projets/p02/ghfc_wgs/Transfert/Dyslexia"
    //sample_path="/mnt/share/Users/Gael/Documents/Git_projects/08002_bourgeron/dataset/test.vcf.gz"
    sample_path="/pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/dataset/Dyslexia.gatk-vqsr.splitted.norm.vep.merged_first_10000.vcf.gz"
    //Warning: do not write the out_path now. See below. If written here, the one below is not considered"
    //fun_path="/mnt/share/Users/Gael/Documents/Git_projects/08002_bourgeron/bin/slivar-functions.js" // functions for slivar
    fun_path="/pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/bin/slivar-functions.js" // functions for slivar
    ped_path="/pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/dataset/Dyslexia.pedigree.txt" // functions for slivar
    annot1_path="/pasteur/zeus/projets/p02/ghfc_wgs_zeus/references/GRCh37/slivar-gnotate/cadd-1.6-SNVs-phred10-GRCh37.zip" //
    annot2_path="/pasteur/zeus/projets/p02/ghfc_wgs_zeus/references/GRCh37/slivar-gnotate/gnomad-2.1.1-genome-GRCh37.zip" //
    affected_patients="C0011JZ C0011K2 C0011K3 C0011KA IP00FNP IP00FNW IP00FNY" // Warning: mandatory space separators
    unaffected_patients="C0011JY C0011K1 C0011K5 C0011KB" // Warning: mandatory space separators
    header_other="CHROM POS ID REF ALT QUAL FILTER INFO FORMAT" // Warning: mandatory space separators
}

//////// end variables that will be used only in the main.nf




//////// variables that will be used below (and potentially in the main.nf file)

//// must be also exported
system_exec = 'local' // the system that runs the workflow. Either 'local' or 'slurm'
//out_path="/mnt/share/Users/Gael/Desktop" // where the report file will be saved. Example report_path = '.' for where the main.nf run is executed or report_path = '/mnt/c/Users/Gael/Desktop'. Warning: this does not work: out_path="/mnt/share/Users/gael/Desktop"
out_path="/pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/results"
//// end must be also exported

//// General variable
result_folder_name="PL_family_WGS"
//// end General variable

//// slurm variables
fastqueue = 'hubbioit' // fast for -p option of slurm. Example: fastqueue = 'common,dedicated'. Example: fastqueue = 'hubbioit'
fastqos= '--qos=fast' // fast for --qos option of slurm. Example: fastqos= '--qos=fast'
normalqueue = 'hubbioit' // normal for -p option of slurm. Example: normalqueue = 'bioevo'
normalqos = '--qos=hubbioit' // normal for --qos option of slurm. Example: normalqos = '--qos=dedicated'
longqueue = 'hubbioit' // slow for -p option of slurm. Example: longqueue = 'bioevo'
longqos = '--qos=hubbioit' // slow for --qos option of slurm. Example: longqos = '--qos=dedicated'
add_options = ' ' // additional option of slurm. Example: addoptions = '--exclude=maestro-1101,maestro-1034' or add_options = ' '
//// end slurm variables

//////// end variables that will be used below



//////// Pre processing

int secs = (new Date().getTime())/1000
out_path="${out_path}/${result_folder_name}_${secs}"

//////// end Pre processing



//////// variables used here and also in the main.nf file

env {
    out_path = "${out_path}"
    system_exec = "${system_exec}"
}

//////// variables used here and also in the main.nf file





//////// Scopes

// kind of execution. Either 'local' or 'slurm'
// those are closures. See https://www.nextflow.io/docs/latest/script.html#closures
executor {
    name = "${system_exec}"
    queueSize = 2000
}

// create a report folder and print a html report file . If no absolute path, will be where the run is executed
// see https://www.nextflow.io/docs/latest/config.html#config-report
report {
       enabled = true
       file = "${out_path}/reports/report.html" // warning: here double quotes to get the nextflow variable interpretation
}

// txt file with all the processes and info
trace {
    enabled = true
    file = "${out_path}/reports/trace.txt"
}

// html file with all the processes
timeline {
    enabled = true
    file = "${out_path}/reports/timeline.html"
}

// .dot picture of the workflow
dag {
    enabled = true
    file = "${out_path}/reports/dag.dot"
}


// define singularity parameters
singularity {
    enabled = true
    autoMounts = true // automatically mounts host paths in the executed container
    //runOptions = '--home $HOME:/home/$USER --bind /pasteur' // provide any extra command line options supported by the singularity exec. HEre, fait un bind de tout /pasteur dans /pasteur du container. Sinon pas d accès
    cacheDir = 'singularity' // name of the directory where remote Singularity images are stored. When rerun, the exec directly uses these without redownloading them. When using a computing cluster it must be a shared folder accessible to all computing nodes
}

//////// end Scopes



//////// directives

// provide the default directives for all the processes in the main.nf pipeline calling this config file
process {
// directives for all the processes
    // executor='local' // no need because already defined above in the executor scope
    if(system_exec == 'slurm'){
        queue = "$fastqueue"
        clusterOptions = "$fastqos $add_options --gres=tmpspace:85G"
        scratch=false
        maxRetries=1
        errorStrategy='retry'
    }

    withLabel: bash {
        container='gmillot/bash-extended_v3.0:gitlab_v4.0'
        cpus=1
        memory='3G'
    }

    withLabel: slivar {
        container='brentp/slivar:v0.2.7'
        cpus=1
        memory='85G'
    }
}

//////// end directives