/*
#########################################################################
##                                                                     ##
##     vcf_slivar_extraction.nf                                        ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################
*/


//////// Arguments of nextflow run

params.modules = ""

//////// end Arguments of nextflow run


//////// Variables

config_file = file("${projectDir}/vcf_slivar_extraction.config")
log_file = file("${launchDir}/.nextflow.log")
modules = params.modules // remove the dot -> can be used in bash scripts

//////// end Variables


//////// Variables from config.file that need to be modified

sample_path_test = file("${sample_path}") // to test if exist below
fun_path_test = file("${fun_path}") // to test if exist below
annot1_path_test = file("${annot1_path}") // to test if exist below
annot2_path_test = file("${annot2_path}") // to test if exist below
ped_path_test = file("${ped_path}") // to test if exist below

//////// end Variables from config.file that need to be modified


//////// Channels

//// used once

vcf_ch1 = Channel.fromPath("${sample_path}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message
fun_ch = Channel.fromPath("${fun_path}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message
annot1_ch = Channel.fromPath("${annot1_path}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message
annot2_ch = Channel.fromPath("${annot2_path}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message
ped_ch = Channel.fromPath("${ped_path}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message

affected_patients_ch = Channel.value("${affected_patients}")
unaffected_patients_ch = Channel.value("${unaffected_patients}")
patient_ch = Channel.value(["${affected_patients}", "${unaffected_patients}"]) // create two entries, see https://www.nextflow.io/docs/latest/channel.html#from
patient_name_ch = Channel.value(["affected_patients", "unaffected_patients"]) // create two entries, see https://www.nextflow.io/docs/latest/channel.html#from

//// end used once

//////// end Channels



//////// Checks

if(system_exec == 'local' || system_exec == 'slurm'){
    def file_exists1 = sample_path_test.exists()
    if( ! file_exists1){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID sample_path PARAMETER IN nextflow.config FILE: ${sample_path}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    def file_exists2 = fun_path_test.exists()
    if( ! file_exists2){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID annot1_path PARAMETER IN nextflow.config FILE: ${annot1_path}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    def file_exists3 = annot1_path_test.exists()
    if( ! file_exists3){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID annot1_path PARAMETER IN nextflow.config FILE: ${annot1_path}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    def file_exists4 = annot2_path_test.exists()
    if( ! file_exists4){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID annot2_path PARAMETER IN nextflow.config FILE: ${annot2_path}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    def file_exists5 = ped_path_test.exists()
    if( ! file_exists5){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID ped_path PARAMETER IN nextflow.config FILE: ${ped_path}\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
}else{
    error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID system_exec PARAMETER IN nextflow.config FILE: ${system_exec}\n\n========\n\n"
}

//////// end Checks



//////// Processes


process WorkflowVersion { // create a file with the workflow version in out_path
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}", mode: 'copy'
    cache 'false'

    output:
    file "Run_info.txt"

    script:
    """
    echo "Project (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} remote -v | head -n 1) > Run_info.txt # works only if the main script run is located in a directory that has a .git folder, i.e., that is connected to a distant repo
    echo "Git info (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} describe --abbrev=10 --dirty --always --tags) >> Run_info.txt # idem. Provide the small commit number of the script and nextflow.config used in the execution
    echo "Cmd line: ${workflow.commandLine}" >> Run_info.txt
    echo "execution mode": ${system_exec} >> Run_info.txt
    modules=$modules # this is just to deal with variable interpretation during the creation of the .command.sh file by nextflow. See also \$modules below
    if [[ ! -z \$modules ]] ; then
        echo "loaded modules (according to specification by the user thanks to the --modules argument of main.nf)": ${modules} >> Run_info.txt
    fi
    echo "Manifest's pipeline version: ${workflow.manifest.version}" >> Run_info.txt
    echo "result path: ${out_path}" >> Run_info.txt
    echo "nextflow version: ${nextflow.version}" >> Run_info.txt
    echo -e "\\n\\nIMPLICIT VARIABLES:\\n\\nlaunchDir (directory where the workflow is run): ${launchDir}\\nprojectDir (directory where the main.nf script is located): ${projectDir}\\nworkDir (directory where tasks temporary files are created): ${workDir}" >> Run_info.txt
    echo -e "\\n\\nUSER VARIABLES:\\n\\nout_path: ${out_path}\\nsample_path: ${sample_path}" >> Run_info.txt
    """
}
//${projectDir} nextflow variable
//${workflow.commandLine} nextflow variable
//${workflow.manifest.version} nextflow variable
//Note that variables like ${out_path} are interpreted in the script block



process slivar {
    label 'slivar' // see the withLabel: bash in the nextflow config file 
    publishDir path: "${out_path}", mode: 'copy', overwrite: false
    cache 'false'

    //no channel input here for the vcf, because I do not transform it
    input:
    file vcf from vcf_ch1
    file fun from fun_ch
    file annot1 from annot1_ch
    file annot2 from annot2_ch
    file ped from ped_ch
    val affected_patients from affected_patients_ch
    val unaffected_patients from unaffected_patients_ch
    // see the scope for the use of affected_patients which is already a variable from .config file

    output:
    file "res.vcf" into vcf_ch2

    script:
    """
    #!/bin/bash -ue
    export TMPDIR="/pasteur/sonic/scratch/users/$USER/$SLURM_JOB_ID"
    echo $TMPDIR
    slivar expr --js $fun -g $annot1 --vcf $vcf --ped $ped --pass-only -o "res.vcf"
    """
    // write between "" (example "$unaffected_patients") to make a single argument when the variable is made of several values separated by a space. Otherwise, several arguments will be considered
    // warning: ${in_path}/${sample_name} instead of ${vcf_gz} does not work, because the file used is not here anymore (do not use path in nextflow)
}




process Backup {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}", mode: 'copy', overwrite: false // since I am in mode copy, all the output files will be copied into the publishDir. See \\wsl$\Ubuntu-20.04\home\gael\work\aa\a0e9a739acae026fb205bc3fc21f9b
    cache 'false'

    input:
    file config_file
    file log_file

    output:
    file "${config_file}" // warning message if we use file config_file
    file "${log_file}" // warning message if we use file log_file
    file "Log_info.txt"

    script:
    """
    echo -e "full .nextflow.log is in: ${launchDir}\nThe one in the result folder is not complete (miss the end)" > Log_info.txt
    """
}


//////// end Processes
