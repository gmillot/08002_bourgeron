Project (empty means no .git folder where the main.nf file is present): 
Git info (empty means no .git folder where the main.nf file is present): 
Cmd line: nextflow run --modules /opt/gensoft/exe/java/13.0.2/bin/java,/opt/gensoft/exe/singularity/3.8.3/bin/singularity,/opt/gensoft/exe/git/2.25.0/bin/git vcf_fisher.nf -c vcf_fisher.config
execution mode: slurm
loaded modules (according to specification by the user thanks to the --modules argument of main.nf): /opt/gensoft/exe/java/13.0.2/bin/java,/opt/gensoft/exe/singularity/3.8.3/bin/singularity,/opt/gensoft/exe/git/2.25.0/bin/git
Manifest's pipeline version: null
result path: /pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/results/PL_family_WGS_fisher_1651770212
nextflow version: 21.04.1


IMPLICIT VARIABLES:

launchDir (directory where the workflow is run): /pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron
projectDir (directory where the main.nf script is located): /pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron
workDir (directory where tasks temporary files are created): /pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/work


USER VARIABLES:

out_path: /pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/results/PL_family_WGS_fisher_1651770212
sample_path: /pasteur/zeus/projets/p01/BioIT/gmillot/08002_bourgeron/dataset/Dyslexia.gatk-vqsr.splitted.norm.vep.merged_first_10000.vcf.gz
